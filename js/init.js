(function($){
  $(function(){

    $('.sidenav').sidenav();
    $('.parallax').parallax();

  }); // end of document ready
})(jQuery); // end of jQuery name space

$(".dropdown-trigger").dropdown();

  var instance = M.Tabs.init();

//   // Or with jQuery

//   $(document).ready(function(){
//     $('.tabs').tabs();
//   });



// $(document).ready(function(){
 
//       $('#demo-carousel').carousel();
 
//     });

//   var instance = M.Carousel.init({
//     fullWidth: true})


    document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.carousel');
    var options = {
      indicators:'true',
      duration: 1000,
      dist: -150,
    };
    var instances = M.Carousel.init(elems, options);
  });