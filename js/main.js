 document.addEventListener('DOMContentLoaded', function() {
 var elems = document.querySelectorAll('select');
 var instances = M.FormSelect.init(elems);
  });
    google.charts.load('current', {packages: ['corechart']});
    google.charts.load('current', {packages: ['gauge']});
    google.charts.setOnLoadCallback(drawChart1);
    google.charts.setOnLoadCallback(drawChart2);
    google.charts.setOnLoadCallback(drawChart3);
    google.charts.setOnLoadCallback(init);
    google.charts.setOnLoadCallback(init1);
    google.charts.setOnLoadCallback(init2);

  function drawChart1() {
      // Define the chart to be drawn.
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Element');
      data.addColumn('number', null);
      data.addRows([
        ['Saulius Svernelis', 25.95],
        ['Ingrida Šimonytė', 19.22],
        ['Gitanas Nausėda', 54.83]
      ]);
  var options = {
      title:'Visuomenės apklausos rezultatai',   
      titleSize: 25,
          height:400,
           pieHole: 0.4,
           fontSize: 16,

           fontName: 'Arial',
      slices: {
            0: { color: "teal", },
            1: { color: "indigo" },
            2: { color: "grey" },
          }

      };

        // Instantiate and draw the chart.
        var chart = new google.visualization.PieChart(document.getElementById('Pyragas'));


        chart.draw(data, options);
  }

 function drawChart2() {
      var data = google.visualization.arrayToDataTable([
        ["Element", "Eur", { role: "style" } ],
        ["Nausėda", 1.94, "grey"],
        ["Skvernelis", 3.49, "teal"],
        ["Šimonytė", 5.223, "indigo"],
        
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "Pajamos per mėn. (tūkst. Eur)",
        fontSize: 16,
        height: 350,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },

      };
      var chart = new google.visualization.ColumnChart(document.getElementById("Stulpeliai"));
      chart.draw(view, options);
  }


  function drawChart3() {

        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['Šimonytė', 20],
          ['Skvernelis', 25],
          ['Nausėda', 35]
        ]);

        var options = {
          indigoFrom: 90, indigoTo: 100,
            tealFrom:75, tealwTo: 90,
            minorTicks: 5
        };

        var chart = new google.visualization.Gauge(document.getElementById('Spidometrai'));

        chart.draw(data, options);

        setInterval(function() {
          data.setValue(0, 1, 20 + Math.round(10 * Math.random()));
          chart.draw(data, options);
        }, 5000 );

       
        setInterval(function() {
          data.setValue(1, 1, 30 + Math.round(10 * Math.random()));
          chart.draw(data, options);
        }, 5000);
        setInterval(function() {
          data.setValue(2, 1, 30 + Math.round(10 * Math.random()));
          chart.draw(data, options);
        }, 5000);
  }
function init() {
    var options = {
      width: 400,
      height: 240,
      animation:{
        duration: 1000,
        easing: 'out',
      },
      vAxis: {minValue:0, maxValue:1000}
    };
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'N');
    data.addColumn('number');
    data.addRow(['Šimonytę parėmė', 0]);

    var chart = new google.visualization.ColumnChart(
        document.getElementById('visualization'));
    var button = document.getElementById('b1');

    function drawChart() {
      // Disabling the button while the chart is drawing.
      button.disabled = true;
      google.visualization.events.addListener(chart, 'ready',
          function() {
            button.disabled = false;
          });
      chart.draw(data, options);
    }

    button.onclick = function() {
      var newValue = 1253 - data.getValue(0, 1);
      data.setValue(0, 1, newValue);
      drawChart();
    }
    drawChart();
  }
 function init1() {
    var options = {
      width: 400,
      height: 240,
      animation:{
        duration: 1000,
        easing: 'out',
      },
      vAxis: {minValue:0, maxValue:1000}
    };
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'N');
    data.addColumn('number');
    data.addRow(['Skvernelį parėmė', 0]);

    var chart = new google.visualization.ColumnChart(
        document.getElementById('visualization1'));
    var button = document.getElementById('b2');

    function drawChart() {
      // Disabling the button while the chart is drawing.
      button.disabled = true;
      google.visualization.events.addListener(chart, 'ready',
          function() {
            button.disabled = false;
          });
      chart.draw(data, options);
    }

    button.onclick = function() {
      var newValue = 666 - data.getValue(0, 1);
      data.setValue(0, 1, newValue);
      drawChart();
    }
    drawChart();
  }
   function init2() {
    var options = {
      width: 400,
      height: 240,
      animation:{
        duration: 1000,
        easing: 'out',
      },
      vAxis: {minValue:0, maxValue:1000}
    };
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'N');
    data.addColumn('number');
    data.addRow(['Nausėdą parėmė', 0]);

    var chart = new google.visualization.ColumnChart(
        document.getElementById('visualization2'));
    var button = document.getElementById('b3');

    function drawChart() {
      // Disabling the button while the chart is drawing.
      button.disabled = true;
      google.visualization.events.addListener(chart, 'ready',
          function() {
            button.disabled = false;
          });
      chart.draw(data, options);
    }

    button.onclick = function() {
      var newValue = 1521 - data.getValue(0, 1);
      data.setValue(0, 1, newValue);
      drawChart();
    }
    drawChart();
  }